import Parser
import Control.Monad
import qualified Data.Map as Map

data Rexpr a = Rval a
             | (:!) (Rexpr a)
             | Rexpr a :|| Rexpr a
             | Rexpr a :&& Rexpr a
             | Rexpr a :^ Rexpr a
                                                    deriving Show

instance Functor Rexpr where
        fmap f (Rval x) = Rval $ f x
        fmap f ((:!) a) = (:!) $ fmap f a
        fmap f (a :|| b) = fmap f a :|| fmap f b
        fmap f (a :&& b) = fmap f a :&& fmap f b
        fmap f (a :^ b) = fmap f a :^ fmap f b

instance Applicative Rexpr where
        pure = return
        (<*>) = ap

instance Monad Rexpr where
        return x = Rval x
        p >>= f = flatten $ fmap f p where
            flatten (Rval x) = x
            flatten ((:!) a) = (:!) $ flatten a
            flatten (a :|| b) = flatten a :|| flatten b
            flatten (a :&& b) = flatten a :&& flatten b
            flatten (a :^ b) = flatten a :^ flatten b

instance Foldable Rexpr where
    foldr f z (Rval x) = f x z
    foldr f z ((:!) a) = foldr f z a
    foldr f z (a :|| b) = foldr f (foldr f z b) a
    foldr f z (a :&& b) = foldr f (foldr f z b) a
    foldr f z (a :^ b) = foldr f (foldr f z b) a

instance Traversable Rexpr where
        traverse f (Rval x) = Rval <$> f x
        traverse f ((:!) a) = (:!) <$> traverse f a
        traverse f (a :|| b) = (:||) <$> traverse f a <*> traverse f b
        traverse f (a :&& b) = (:&&) <$> traverse f a <*> traverse f b
        traverse f (a :^ b) = (:^) <$> traverse f a <*> traverse f b

parse_boolexpr :: Parser a -> Parser (Rexpr a)
parse_boolexpr p = term
    where 
          val = fmap Rval p
          term = factorNot `chainl1` (andOp +++ orOp +++ xorOp)
          andOp = do symb "&&" ; return (:&&)
          orOp = do symb "||" ; return (:||)
          xorOp = do symb "^" ; return (:^)
          factorNot = do 
                         f <- notOp
                         x <- do { symb "(" ; e <- parse_boolexpr p ; symb ")" ; return e } +++ val
                         return (f x)
          notOp = do 
                     n <- ntoken (symb "!")
                     if odd n then return (:!)
                              else return id


parse_rexpr :: Parser (Rexpr String)
parse_rexpr = parse_boolexpr $ token word



solve_boolexpr :: Rexpr Bool -> Bool
solve_boolexpr (Rval x) = x
solve_boolexpr ((:!) a) = not $ solve_boolexpr a
solve_boolexpr (a :|| b) = solve_boolexpr a || solve_boolexpr b
solve_boolexpr (a :&& b) = solve_boolexpr a && solve_boolexpr b
solve_boolexpr (a :^ b) = solve_boolexpr a /= solve_boolexpr b

generate_propositions :: Rexpr String -> [Rexpr (String,Bool)]
generate_propositions = mapM $ \x -> [(x,False),(x,True)]

get_verities expr = filter (\x -> solve_boolexpr (fmap snd x)) xs where
    xs = generate_propositions expr



test str = mapM_ (putStrLn . show) $ get_verities . fst . head $ apply (parse_rexpr) str

