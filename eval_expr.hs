import Parser
import qualified Data.Map as Map

expr_double :: Parser Double
expr_double = term `chainl1` (do { many1 $ symb "+" ; return (+) } +++ notOp )
    where 
          val = doublenumber
          notOp = do { n <- ntoken $ char '-' ; if odd n then return (-) else return (+) }
          term = factor `chainl1` (do { symb "*" ; return (*) } +++ do { symb "/" ; return (/) })
          factor = val +++ do 
                              n <- ntoken (char '-')
                              symb "("
                              e <- expr_double
                              symb ")"
                              if odd n then return (negate e)
                                       else return e

expr_int :: Parser Integer
expr_int = term `chainl1` (do { many1 $ symb "+" ; return (+) } +++ notOp )
    where 
          val = integernumber
          notOp = do { n <- ntoken $ char '-' ; if odd n then return (-) else return (+) }
          term = factor `chainl1` (do { symb "*" ; return (*) } +++ do { symb "/" ; return (div) })
          factor = val +++ 
                            do 
                              n <- ntoken (char '-')
                              symb "("
                              e <- expr_int
                              symb ")"
                              if odd n then return (negate e)
                                       else return e

expr_compare :: Parser Bool
expr_compare = do 
                  a <- expr_double
                  f <- getComparison
                  b <- expr_double
                  return (f a b)
                  where 
                        getComparison = leOp +++ geOp +++ ltOp +++ gtOp +++ neqOp +++ eqOp
                        geOp = do symb ">=" ; return (>=)
                        leOp = do symb "<=" ; return (<=)
                        ltOp = do symb "<" ; return (<)
                        gtOp = do symb ">" ; return (>)
                        neqOp = do symb "!=" ; return (/=)
                        eqOp = do symb "==" ; return (==)

expr_bool :: Parser Bool
expr_bool = term
    where 
          val = bool
          term = factorNot `chainl1` (andOp +++ orOp)
          andOp = do symb "&&" ; return (&&)
          orOp = do symb "||" ; return (||)
          factorNot = do 
                         f <- notOp
                         x <- val +++ expr_compare +++ do { symb "(" ; e <- expr_bool ; symb ")" ; return e }
                         return (f x)
          notOp = do 
                     n <- ntoken (symb "!")
                     if odd n then return not
                              else return id


