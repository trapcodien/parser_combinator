# Parser combinator #
* For test in ghci, type ':load Parser.hs' before import
* Please take a look to eval_expr.hs and eval_bool.hs for further examples.

### Some Examples ###

```haskell
import Parser

runParser getch "Hello World"
-- [('H',"ello World")]
-- it :: [(Char, String)]

runParser word "BONG!"
-- [("BONG","!")]
-- it :: [(String, String)]

runParser space "       Hello World"
-- [("       ","Hello World")]
-- it :: [(String, String)]

runParser (word `sepby` space) "Hello   \t \t \n    World   \n\t \t \n  ! !! !!! !! !   Yolo"
-- [(["Hello","World",""],"! !! !!! !! !   Yolo")]
-- it :: [([String], String)]

runParser (word `sepby` ( many1 (sat isSpace +++ char '!') )) "Hello   \t \t \n    World   \n\t \t \n  ! !! !!! !! !   Yolo"
-- [(["Hello","World","Yolo"],"")]
-- it :: [([String], String)]

```
