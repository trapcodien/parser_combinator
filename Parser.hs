{-------------------------------------------------------------------------------
      Author: Guillaume ARM             Email: trapcodien[at]hotmail[dot]fr     
--------------------*******************************-----------------------------
--------------------|  Monadic parser combinator  |-----------------------------
--------------------*******************************----------------------------}

module Parser where

import qualified Control.Applicative as CA
import qualified Data.Map as Map
import Control.Monad
import Data.Char
----------------------- Monad implementation -----------------------------------
newtype Parser a = Parser { runParser :: (String -> Maybe (a,String)) }

instance Monad Parser where
        return x = Parser $ \str -> return (x,str)
        p >>= f = Parser $ \str -> do 
                                      (a,str') <- runParser p str
                                      runParser (f a) str'

instance MonadPlus Parser where
        mzero = Parser $ \str -> mzero
        p `mplus` q = Parser $ \str -> runParser p str `mplus` runParser q str

instance CA.Alternative Parser where
        empty = mzero
        (<|>) = mplus

instance Functor Parser where
        fmap = liftM

instance CA.Applicative Parser where
        pure = return
        (<*>) = ap

---------------------- High order combinators ----------------------------------
apply p str = runParser ( do many . sat $ isSpace ; p ) str

(+++) :: Parser a -> Parser a -> Parser a
(+++) = mplus

many :: Parser a -> Parser [a]
many1 :: Parser a -> Parser [a]
many p = many1 p +++ return []
many1 p = do 
             x <- p
             xs <- many p
             return (x:xs)

sepby :: Parser a -> Parser b -> Parser [a]
sepby1 :: Parser a -> Parser b -> Parser [a]
p `sepby` sep = p `sepby1` sep +++ return []
p `sepby1` sep = do 
                    x <- p
                    xs <- many (sep >> p)
                    return (x:xs)

chainl :: Parser a -> Parser (a -> a -> a) -> a -> Parser a
chainl p op a = p `chainl1` op +++ return a

chainl1 :: Parser a -> Parser (a -> a -> a) -> Parser a
p `chainl1` op = do 
                    a <- p
                    repeat a
                    where 
                          repeat a = (do 
                                        f <- op
                                        b <- p
                                        repeat (f a b)) +++ return a

---------------------- Lexical combinators -------------------------------------
getch :: Parser Char
getch = Parser $ \str -> case str of
                             "" -> mzero
                             (x:xs) -> return (x,xs)
consume :: Parser ()
consume = getch >> return ()

sat :: (Char -> Bool) -> Parser Char
sat p = do 
           c <- getch
           if p c then return c
                  else mzero

char :: Char -> Parser Char
char c = sat  (== c)

nchar :: Char -> Parser Int
nchar c = do 
             str <- many (char c)
             return $ length str

letter :: Parser Char
letter = sat isLetter

token :: Parser a -> Parser a
token p = do 
             x <- p
             many (sat isSpace)
             return x

ntoken :: Parser a -> Parser Int
ntoken p = do 
              xs <- many (token p)
              return $ length xs

strnumber :: Parser String
strnumber = (do 
                  n1 <- num
                  char '.'
                  n2 <- num
                  return (n1 ++ "." ++ n2) ) +++ num
            where num = token . many1 . sat $ isDigit

strnegnumber :: Parser String
strnegnumber = do 
               n <- ntoken (char '-')
               x <- strnumber
               if odd n then return $ '-':x
                        else return x

integernumber :: Parser Integer
integernumber = fmap (read :: String -> Integer) strnegnumber

intnumber :: Parser Int
intnumber = fmap (read :: String -> Int) strnegnumber

floatnumber :: Parser Float
floatnumber = fmap (read :: String -> Float) strnegnumber

doublenumber :: Parser Double
doublenumber = fmap (read :: String -> Double) strnegnumber

string :: String -> Parser String
string "" = return ""
string (x:xs) = do 
                c <- char x
                str <- string xs
                return (c:str)

lowerstring :: String -> Parser String
lowerstring "" = return ""
lowerstring (x:xs) = do 
                        c <- char (toLower x) +++ char (toUpper x)
                        str <- lowerstring xs
                        return (toUpper c : map toLower str)

word :: Parser String
word = many letter

space :: Parser String
space = many1 (sat isSpace)

symb :: String -> Parser String
symb = token . string

lowersymb :: String -> Parser String
lowersymb = token . lowerstring

bool :: Parser Bool
bool = do 
          str <- lowersymb "True" +++ lowersymb "False"
          return (read str :: Bool)

withMap :: Parser String -> Map.Map String a -> Parser a
p `withMap` m = do 
                   str <- p
                   if Map.member str m then return $ (Map.!) m str
                                       else mzero


