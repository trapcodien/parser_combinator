import Parser

type VarName = String

data IntExpr = IntVal Integer
             | IntVar VarName
             | IntNeg IntExpr
             | IntExpr :+ IntExpr
             | IntExpr :- IntExpr
             | IntExpr :* IntExpr
             | IntExpr :/ IntExpr 
                                    deriving Show

data DoubleExpr = DoubleVal Double
                | DoubleVar VarName
                | DoubleNeg DoubleExpr
                | DoubleExpr :+. DoubleExpr
                | DoubleExpr :-. DoubleExpr
                | DoubleExpr :*. DoubleExpr
                | DoubleExpr :/. DoubleExpr 
                                    deriving Show

data BoolExpr = BoolVal Bool
              | BoolVar VarName
              | DoubleExpr :< DoubleExpr
              | DoubleExpr :> DoubleExpr
              | DoubleExpr :<= DoubleExpr
              | DoubleExpr :>= DoubleExpr
              | DoubleExpr :== DoubleExpr
              | DoubleExpr :!= DoubleExpr
              | BoolNot BoolExpr
              | BoolExpr :&& BoolExpr
              | BoolExpr :|| BoolExpr
                                    deriving Show

expr_double :: Parser DoubleExpr
expr_double = term `chainl1` (do { many1 $ symb "+" ; return (:+.) } +++ notOp )
    where 
          varname = (fmap DoubleVar (token $ many1 letter))
          val = fmap DoubleVal doublenumber
          negate = DoubleNeg
          notOp = do { n <- ntoken $ char '-' ; if odd n then return (:-.) else return (:+.) }
          term = factor `chainl1` (do { symb "*" ; return (:*.) } +++ do { symb "/" ; return (:/.) })
          factor = val +++ varname +++ do 
                              n <- ntoken (char '-')
                              symb "("
                              e <- expr_double
                              symb ")"
                              if odd n then return (negate e)
                                       else return e

expr_int :: Parser IntExpr
expr_int = term `chainl1` (do { many1 $ symb "+" ; return (:+) } +++ notOp )
    where 
          varname = fmap IntVar (token $ many1 letter)
          val = fmap IntVal integernumber
          negate = IntNeg
          notOp = do { n <- ntoken $ char '-' ; if odd n then return (:-) else return (:+) }
          term = factor `chainl1` (do { symb "*" ; return (:*) } +++ do { symb "/" ; return (:/) })
          factor = val +++ varname +++ 
                            do 
                              n <- ntoken (char '-')
                              symb "("
                              e <- expr_int
                              symb ")"
                              if odd n then return (negate e)
                                       else return e

expr_compare :: Parser BoolExpr
expr_compare = do 
                  a <- expr_double
                  f <- getComparison
                  b <- expr_double
                  return (f a b)
                  where 
                        getComparison = leOp +++ geOp +++ ltOp +++ gtOp +++ neqOp +++ eqOp
                        geOp = do symb ">=" ; return (:>=)
                        leOp = do symb "<=" ; return (:<=)
                        ltOp = do symb "<" ; return (:<)
                        gtOp = do symb ">" ; return (:>)
                        neqOp = do symb "!=" ; return (:!=)
                        eqOp = do symb "==" ; return (:==)

expr_bool :: Parser BoolExpr
expr_bool = term
    where 
          varname = fmap BoolVar (token $ many1 letter )
          val = fmap BoolVal bool
          not = BoolNot
          term = factorNot `chainl1` (andOp +++ orOp)
          andOp = do symb "&&" ; return (:&&)
          orOp = do symb "||" ; return (:||)
          factorNot = do 
                         f <- notOp
                         x <- val +++ varname +++ expr_compare +++ do { symb "(" ; e <- expr_bool ; symb ")" ; return e }
                         return (f x)
          notOp = do 
                     n <- ntoken (symb "!")
                     if odd n then return not
                              else return id


